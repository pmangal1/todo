items = []
i = 1;
function showItems() {
    document.getElementById('show-items').innerHTML = ''
    items.map(item => {
        let clr = item.completed == 1 ? 'success' : ''
        let status = item.completed == 1 ? 'UnDone':'Done'
        let statusColor = item.completed == 1 ? 'danger':'success'
        document.getElementById('show-items').innerHTML += `<tr class="table-${clr}">
       <td>${item.name}</td>
       <td><button type="button" class="btn btn-primary" onclick="edit('${item.id}','${item.name}')">Edit</button></td>
       <td><button type="button" class="btn btn-${statusColor}" id="d-${item.id}" onclick="status('${item.id}')">${status}</button></td>
       <td><input class="form-check-input" type="checkbox" id="${item.id}" onclick="changeStatus(this)" value="${item.id}" id="defaultCheck1"></td>
     </tr>`;
    })
    updateCheckBoxes();
}

function updateCheckBoxes() {
    items.map(i => {
        if (i.deleted == 1) {
            document.getElementById(i.id).checked = true;
        }
    })
}

function addItem() {
    let item = document.getElementById('add-item').value;
    document.getElementById('view-items').style.display = 'block';
    items.push({
        id: 'id' + i,
        name: item,
        completed: 0,
        deleted: 0
    });
    i++;
    showItems()
    document.getElementById('add-item').value = '';
    return false;
}

function changeStatus(checkbox) {
    let id = checkbox.value
    items.map(item => {
        if (id == item.id) {
            item.deleted = (item.deleted == 0 ? 1 : 0)
        }
    })
}

function status(id) {
    items.map(i => {
        if (i.id == id) {
            i.completed = (i.completed == 1 ? 0 : 1);
        }
    })
    showItems()
}

function edit(id,name){

let edited = prompt("Edit Your Task", name);
if(edited == ''){
    alert('Please Add Text Correctly')
}else{

items.map(i=>{
    if(i.id == id){
        i.name = edited
    }
})
showItems()
}
}


function deleteCheckedItems(){
    items = items.filter(item=>item['deleted'] != 1)
    showItems()
}

function deleteAll(){
    items = []
    showItems()
}